<?php


namespace App\Shared\Library\Cleaning\Algorithm;


use App\Shared\Library\Cleaning\CleaningStrategy;
use App\Shared\Library\Exception\InvalidLastCleanException;

/**
 * Class FCCleaningAlgorithm
 * @package App\Shared\Library\Cleaning\Algorithm
 */
class FCCleaningAlgorithm implements CleaningStrategy
{

    /**
     * @param $car
     * @param array $pods
     * @param array $classes
     * @param array $settings
     * @return int
     * @throws InvalidLastCleanException
     */
    public function calculateNextClean($car, $pods = array(), $classes = array(), $settings= array())
    {
        if ($car['last_clean'] < 0) {
            throw new InvalidLastCleanException;
        }
        $result = 7 - $car['last_clean'];
        return ($result > 0 )?($result>7)?0:$result:0;
    }
}