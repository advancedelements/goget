<?php


namespace App\Shared\Library\Cleaning\Algorithm;

use App\Shared\Library\Cleaning\CleaningStrategy;
use App\Shared\Library\Exception\MaxFrequencyException;
use App\Shared\Library\Exception\MinFrequencyException;

/**
 * Class GCCleaningAlgorithm
 * @package App\Shared\Library\Cleaning\Algorithm
 */
class GCCleaningAlgorithm implements CleaningStrategy
{

    /**
     * @param $car
     * @param $pods
     * @param $classes
     * @param $settings
     * @return float|int|mixed
     * @throws MaxFrequencyException
     * @throws MinFrequencyException
     */
    public function calculateNextClean($car, $pods, $classes, $settings)
    {
        $multipliers = array();
        $multipliers[] = $settings['std_freq'];
        if (isset($pods[$car['pod_id']]) && $pods[$car['pod_id']] === true) {
            $multipliers[] = $settings['dirty_pod'];
        }

        if ( isset($classes[$car['class_id']]) ) {
            $multipliers[] = $classes[$car['class_id']];
        }

        $result = 1;
        foreach ($multipliers as $multiplier) {
            $result *= $multiplier;
        }

        if ($result < $settings['min_freq']) {
            throw new MinFrequencyException();
        }

        if ($result > $settings['max_freq']) {
            throw new MaxFrequencyException();
        }

        $result = round($result - $car['last_clean']);

        return $result;
    }
}