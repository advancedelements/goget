<?php


namespace App\Shared\Library\Cleaning;


use App\Shared\Library\Exception\MaxFrequencyException;
use App\Shared\Library\Exception\MinFrequencyException;

/**
 * Class CleaningFacade
 * @package App\Shared\Library\Cleaning
 */
class CleaningFacade
{
    /**
     * @param $car
     * @param $pods
     * @param $classes
     * @param $settings
     * @param $cleaning_method
     * @return mixed
     */
    public function entry($car, $pods, $classes, $settings, $cleaning_method)
    {
        $cleaningStrategy = new CleaningStrategyContext();

        $cleaningStrategy->createCleaningStrategy($car['id'], $cleaning_method);
        $nextClean = $cleaningStrategy->calculateNextClean($car, $pods, $classes, $settings);

        return $nextClean;
    }
}