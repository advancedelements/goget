<?php


namespace App\Shared\Library\Cleaning;


use App\Shared\Library\Cleaning\Algorithm;
use App\Shared\Library\Exception\InvalidCleaningStrategyException;
use App\Shared\Library\Exception\InvalidLastCleanException;
use App\Shared\Library\Exception\MaxFrequencyException;
use App\Shared\Library\Exception\MinFrequencyException;

/**
 * Class CleaningStrategyContext
 * @package App\Shared\Library\Cleaning
 */
class CleaningStrategyContext implements CleaningStrategy
{
    /**
     * @var
     */
    private $strategy;

    /**
     * @param $carId
     * @param $cleaningMethod
     * @throws InvalidCleaningStrategyException
     */
    public function createCleaningStrategy($carId,  $cleaningMethod)
    {
        $cleaningStrategyMethod = $cleaningMethod[$carId];

        switch ($cleaningStrategyMethod) {
            case self::GG_CLEANING_ALGORITHM:
                $this->strategy = new Algorithm\GCCleaningAlgorithm();
                break;
            case self::FC_CLEANING_ALGOIRTHM:
                $this->strategy = new Algorithm\FCCleaningAlgorithm();
                break;
            default:
                throw new InvalidCleaningStrategyException();
        }
    }

    /**
     * @param $car
     * @param $pods
     * @param $classes
     * @param $settings
     * @return int
     */
    public function calculateNextClean($car, $pods, $classes, $settings)
    {
        if ($this->strategy instanceof Algorithm\GCCleaningAlgorithm) {
            try {
                return $this->strategy->calculateNextClean($car, $pods, $classes, $settings);
            } catch (MinFrequencyException $e) {
                return $settings['min_freq'] - $car['last_clean'];
            } catch (MaxFrequencyException $e) {
                return  $settings['max_freq'] - $car['last_clean'];
            }
        } else if ($this->strategy instanceof Algorithm\FCCleaningAlgorithm) {
            try {
                return $this->strategy->calculateNextClean($car, $pods, $classes, $settings);
            } catch (InvalidLastCleanException $e) {
                error_log("Car with car_id ".$car['id']." has got an incompatible last_clean value. Please check");
                return 7;
            }
        }
    }
}