<?php


namespace App\Shared\Library\Cleaning;


/**
 * Interface CleaningStrategy
 * @package App\Shared\Library\Cleaning
 */
interface CleaningStrategy
{

    const GG_CLEANING_ALGORITHM = 1;

    const FC_CLEANING_ALGOIRTHM = 2;

    /**
     * @param $car
     * @param $pods
     * @param $classes
     * @param $settings
     * @return mixed
     */
    public function calculateNextClean($car, $pods, $classes, $settings);
}