<?php


namespace App\Shared\Library\Exception;


/**
 * Class InvalidCleaningStrategyException
 * @package App\Shared\Library\Exception
 */
class InvalidCleaningStrategyException extends BaseException
{

}