<?php


namespace App\Shared\Library\Exception;


/**
 * Class InvalidLastCleanException
 * @package App\Shared\Library\Exception
 */
class InvalidLastCleanException extends BaseException
{

}