<?php


namespace App\Http\Controllers;


use App\Shared\Library\Cleaning\CleaningFacade;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cleaning()
    {
        $response = array();

        $response['data'] = "data";

        $pods = array(11 => true, 12 => false);
        $classes = array(1 => 0.7, 2 => 1.0, 3 => 1.5);
        $settings = array(
            'dirty_pod' => 0.9,
            'min_freq' => 7,
            'std_freq' => 14,
            'max_freq' => 28,
        );
        $cleaning_method = array(7 => 1, 8 => 1, 9 => 2, 10 => 2);

        $cars = array();
        $cars[] = array('id' => 7, 'pod_id' => 11,
            'class_id' => 3, 'last_clean' => 5);
        $cars[] = array('id' => 8, 'pod_id' => 12,
            'class_id' => 1, 'last_clean' => 7);
        $cars[] = array('id' => 9, 'pod_id' => 13,
            'class_id' => 3, 'last_clean' => 7);
        $cars[] = array('id' => 10, 'pod_id' => 14,
            'class_id' => 2, 'last_clean' => 3);

        $response['title'] = 'Cleaning Schedule';
        $response['scheduleData'] = [];

        foreach ($cars as $car) {
            $facade = new CleaningFacade();
            $days_left = $facade->entry($car, $pods, $classes, $settings, $cleaning_method);
            if ($days_left == 0) {
                $urgency_colour = "red";
            } else if ($days_left<=3) {
                $urgency_colour = "yellow";
            } else {
                $urgency_colour = "white";
            }
            $response['scheduleData'][] = array('car_id' => $car['id'], 'days_left' => $days_left , 'urgency' => $urgency_colour);
        }

        $response['settings'] = $settings;

        return view('home', $response);
    }
}