<?php

namespace Tests\Unit\Shared\Library\Cleaning\Algorithm;

use App\Shared\Library\Cleaning\Algorithm\FCCleaningAlgorithm;
use App\Shared\Library\Exception\InvalidLastCleanException;
use Tests\TestCase;

/**
 * Class FCCleaningAlgorithmTest
 * @package Tests\Unit\Shared\Library\Cleaning\Algorithm
 */
class FCCleaningAlgorithmTest extends TestCase
{
    /**
     * Testing when last_clean is 5 expects 2
     */
    public function testPositiveLastClean()
    {
        $car = array('id' => 7, 'pod_id' => 11,
            'class_id' => 3, 'last_clean' => 5);

        $algorithm = new FCCleaningAlgorithm();

        $this->assertEquals(2, $algorithm->calculateNextClean($car));
    }

    /**
     * Testing when last_clean is more than 7 days expects 0
     */
    public function testLargeLastClean()
    {
        $car = array('id' => 7, 'pod_id' => 11,
            'class_id' => 3, 'last_clean' => 12);

        $algorithm = new FCCleaningAlgorithm();

        $this->assertEquals(0, $algorithm->calculateNextClean($car));
    }

    /**
     * Testing when last_clean is 0 expects 7
     */
    public function testZeroLastClean()
    {
        $car = array('id' => 7, 'pod_id' => 11,
            'class_id' => 3, 'last_clean' => 0);

        $algorithm = new FCCleaningAlgorithm();

        $this->assertEquals(7, $algorithm->calculateNextClean($car));
    }

    /**
     * Testing when last_clean is greater than the max frequency for FCCleaningAlgorithm expects 0
     */
    public function testLastCleanGreaterThanSeven()
    {
        $car = array('id' => 7, 'pod_id' => 11,
            'class_id' => 3, 'last_clean' => 20);

        $algorithm = new FCCleaningAlgorithm();

        $this->assertEquals(0, $algorithm->calculateNextClean($car));
    }

    /**
     * Testing when last_clean is a negative number expects InvalidLastCleanException
     *
     * @expectedException  InvalidLastCleanException
     */
    public function testNegativeLastClean()
    {
        $this->expectException(\App\Shared\Library\Exception\InvalidLastCleanException::class);
        $car = array('id' => 7, 'pod_id' => 11,
            'class_id' => 3, 'last_clean' => -5);

        $algorithm = new FCCleaningAlgorithm();
        $algorithm->calculateNextClean($car);
    }
}
