<?php

namespace Tests\Unit\Shared\Library\Cleaning\Algorithm;

use App\Shared\Library\Cleaning\Algorithm\GCCleaningAlgorithm;
use Tests\TestCase;

/**
 * Class GCCleaningAlgorithmTest
 * @package Tests\Unit\Shared\Library\Cleaning\Algorithm
 */
class GCCleaningAlgorithmTest extends TestCase
{
    /**
     * @dataProvider provider
     */
    public function testPositiveCases($car, $pods, $classes, $settings, $expected)
    {
        $algorithm = new GCCleaningAlgorithm();
        $algorithm->calculateNextClean($car, $pods, $classes, $settings);

        $actual = $algorithm->calculateNextClean($car, $pods, $classes, $settings);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Test to check if MaxFrequencyException and MinFrequencyException are thrown
     * @dataProvider exceptionProvider
     * @expectedException
     */
    public function testExceptionCases($car, $pods, $classes, $settings, $exception)
    {
        $this->expectException($exception);
        $algorithm = new GCCleaningAlgorithm();
        $algorithm->calculateNextClean($car, $pods, $classes, $settings);

        $algorithm->calculateNextClean($car, $pods, $classes, $settings);
    }

    /**
     * @return array
     */
    public function provider()
    {
        $pods = array(11 => true, 12 => false);
        $classes = array(1 => 0.7, 2 => 1.0, 3 => 1.5);
        $settings = array(
            'dirty_pod' => 0.9,
            'min_freq' => 7,
            'std_freq' => 14,
            'max_freq' => 28,
        );

        $car1 = array('id' => 7, 'pod_id' => 11,
            'class_id' => 3, 'last_clean' => 5);

        $car2 = array('id' => 8, 'pod_id' => 12,
            'class_id' => 3, 'last_clean' => 5);

        $car3 = array('id' => 9, 'pod_id' => null,
            'class_id' => 3, 'last_clean' => 5);

        $car4 = array('id' => 7, 'pod_id' => 11,
            'class_id' => 7, 'last_clean' => 5);

        return array(
            array($car1, $pods ,$classes, $settings, 14),
            array($car2, $pods ,$classes, $settings, 16), // false pod_id vlaue
            array($car3, $pods ,$classes, $settings, 16), // Null pod_id Value
            array($car4, $pods ,$classes, $settings, 8), // class_id invalid
        );
    }

    /**
     * @return array
     */
    public function exceptionProvider(){

        $pods = array(11 => true, 12 => false);
        $classes = array(1 => 0.07, 2 => 1.0, 3 => 1.5, 4 => 100);
        $settings = array(
            'dirty_pod' => 0.9,
            'min_freq' => 7,
            'std_freq' => 14,
            'max_freq' => 28,
        );

        $car1 = array('id' => 9, 'pod_id' => 11,
            'class_id' => 4, 'last_clean' => 5);
        $car2 = array('id' => 9, 'pod_id' => 11,
            'class_id' => 1, 'last_clean' => 5);


        return array(
            array($car1, $pods ,$classes, $settings, \App\Shared\Library\Exception\MaxFrequencyException::class), //Exceeds Max frequency
            array($car2, $pods ,$classes, $settings, \App\Shared\Library\Exception\MinFrequencyException::class)  // missed Min frequency
        );
    }
}
