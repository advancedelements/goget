<?php

namespace Tests\Unit\Shared\Library\Cleaning;

use App\Shared\Library\Cleaning\CleaningStrategyContext;
use App\Shared\Library\Exception\InvalidCleaningStrategyException;
use Tests\TestCase;

/**
 * Class CleaningStrategyContextTest
 * @package Tests\Unit\Shared\Library\Cleaning
 */
class CleaningStrategyContextTest extends TestCase
{

    /**
     * @dataProvider provider
     * Test to check correct Algorithm is chosen
     */
    public function testCorrectAlgorithmIsChosen($car, $pods, $classes, $settings, $cleaningMethod, $expected, $notExpected)
    {
        $context = new CleaningStrategyContext();
        $context->createCleaningStrategy($car['id'], $cleaningMethod);

        $actual = $context->calculateNextClean($car, $pods, $classes, $settings);

        $this->assertEquals($expected, $actual);
        $this->assertNotEquals($notExpected,$actual);
    }

    /**
     * @return array
     */
    public function provider()
    {
        $pods = array(11 => true, 12 => false);
        $classes = array(1 => 0.7, 2 => 1.0, 3 => 1.5);
        $settings = array(
            'dirty_pod' => 0.9,
            'min_freq' => 7,
            'std_freq' => 14,
            'max_freq' => 28,
        );
        $cleaningMethod = array(7 => 1, 8 => 1, 9 => 2, 10 => 2);

        $car1 = array('id' => 7, 'pod_id' => 11,
            'class_id' => 3, 'last_clean' => 5);
        $car2 = array('id' => 8, 'pod_id' => 12,
            'class_id' => 1, 'last_clean' => 7);
        $car3 = array('id' => 9, 'pod_id' => 13,
            'class_id' => 3, 'last_clean' => 7);
        $car4 = array('id' => 10, 'pod_id' => 14,
            'class_id' => 2, 'last_clean' => 3);

        return array(
            array($car1, $pods ,$classes, $settings, $cleaningMethod, 14, 2),
            array($car2, $pods ,$classes, $settings, $cleaningMethod, 3, 0),
            array($car3, $pods ,$classes, $settings, $cleaningMethod, 0, 14),
            array($car4, $pods ,$classes, $settings, $cleaningMethod, 4, 11),
        );

    }

    /**
     * Testing when cleaning method value InvalidCleaningStrategyException
     *
     * @expectedException InvalidCleaningStrategyException
     */
    public function testInvalidCleaningMethodValue()
    {
        $this->expectException(\App\Shared\Library\Exception\InvalidCleaningStrategyException::class);
        $car1 = array('id' => 7, 'pod_id' => 11,
            'class_id' => 3, 'last_clean' => 5);
        $cleaningMethod = array(7 => 3, 8 => 1, 9 => 2, 10 => 2);

        $context = new CleaningStrategyContext();
        $context->createCleaningStrategy($car1['id'], $cleaningMethod);
    }

    /**
     * Test to check if a valid integer is returned if Exceptions are thrown
     * @dataProvider outOfFrequencyBoundsProvider
     */
    public function testOutOfFrequencyBounds($car , $pods, $classes,$settings,$expected){

        $cleaningMethod = array(7 => 1, 9 => 1, 10 =>  2);

        $context = new CleaningStrategyContext();
        $context->createCleaningStrategy($car['id'], $cleaningMethod);

        $actual = $context->calculateNextClean($car, $pods, $classes, $settings);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array
     */
    public function outOfFrequencyBoundsProvider(){
        $pods = array(11 => true, 12 => false);
        $classes = array(1 => 0.07, 2 => 1.0, 3 => 1.5, 4 => 100);
        $settings = array(
            'dirty_pod' => 0.9,
            'min_freq' => 7,
            'std_freq' => 14,
            'max_freq' => 28,
        );

        $car1 = array('id' => 9, 'pod_id' => 11,
            'class_id' => 4, 'last_clean' => 5);
        $car2 = array('id' => 9, 'pod_id' => 11,
            'class_id' => 1, 'last_clean' => 5);
        $car3 = array('id' => 10, 'pod_id' => 11,
            'class_id' => 1, 'last_clean' => -5);


        return array(
            array($car1, $pods ,$classes, $settings, 23), //Exceeds Max frequency
            array($car2, $pods ,$classes, $settings, 2),  // Missed Min frequency
            array($car3, $pods ,$classes, $settings, 7) // FC Exceeds 7 days
        );
    }
}
